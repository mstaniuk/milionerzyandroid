package io.ss.milionerzy;

import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Random;

public class GameActivity extends AppCompatActivity implements View.OnClickListener {
    private FloatingActionButton polNaPolBtn;
    private FloatingActionButton pytanieDoPublicznosciBtn;
    private FloatingActionButton teefonDoPrzyjacielaBtn;

    private TextView questionTxt;
    private TextView currentText;
    private TextView currentWinTxt;

    private Button odpABtn;
    private Button odpBBtn;
    private Button odpCBtn;
    private Button odpDBtn;

    private boolean isPolNaPolAvailable = true;
    private boolean isPytanieDoPublicznosciAvailable = true;
    private boolean isTelefonDoPrzyjacielaAvailable = true;

    private char userAnswer;
    private Question currentQuestion;
    private Integer currentPrize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);

        polNaPolBtn = (FloatingActionButton) findViewById(R.id.polNaPolBtn);
        polNaPolBtn.setOnClickListener(this);
        polNaPolBtn.setVisibility(View.VISIBLE);
        pytanieDoPublicznosciBtn = (FloatingActionButton) findViewById(R.id.pytanieDoPublicznosciBtn);
        pytanieDoPublicznosciBtn.setOnClickListener(this);
        pytanieDoPublicznosciBtn.setVisibility(View.VISIBLE);
        teefonDoPrzyjacielaBtn = (FloatingActionButton) findViewById(R.id.teefonDoPrzyjacielaBtn);
        teefonDoPrzyjacielaBtn.setOnClickListener(this);
        teefonDoPrzyjacielaBtn.setVisibility(View.VISIBLE);

        questionTxt = (TextView) findViewById(R.id.questionTxt);
        currentText = (TextView) findViewById(R.id.currentText);
        currentWinTxt = (TextView) findViewById(R.id.currentWinTxt);

        odpABtn = (Button) findViewById(R.id.odpABtn);
        odpABtn.setOnClickListener(this);

        odpBBtn = (Button) findViewById(R.id.odpBBtn);
        odpBBtn.setOnClickListener(this);

        odpCBtn = (Button) findViewById(R.id.odpCBtn);
        odpCBtn.setOnClickListener(this);

        odpDBtn = (Button) findViewById(R.id.odpDBtn);
        odpDBtn.setOnClickListener(this);

        getQuestion();
        setPrizes();
    }

    private void setButtonPropose(Button btn) {
        btn.setBackgroundColor(Color.parseColor("#25ffff00"));
    }

    private void setButtonUnactive(Button btn) {
        btn.setBackgroundColor(Color.parseColor("#25ffffff"));
    }

    private void setButtonNormal(Button btn) {
        btn.setBackgroundColor(Color.parseColor("#a4000000"));
    }

    private void setPrizes() {
        if(currentPrize == 0 ||currentPrize == 1000 || currentPrize == 32000 || currentPrize == 1000000) {
            currentWinTxt.setText(String.valueOf(currentPrize));
        }

        currentText.setText(String.valueOf(currentPrize));
    }

    private Button getRandomButton() {
        ArrayList<Button> a = new ArrayList<Button>();
        a.add(odpABtn);
        a.add(odpBBtn);
        a.add(odpCBtn);
        a.add(odpDBtn);

        return a.get(new Random().nextInt(a.size()));
    }

    private int getRandomListIndex(ArrayList<Button> l) {
        return new Random().nextInt(l.size());
    }

    private void setPolNaPol() {
        int index = 0;
        ArrayList<Button> a = new ArrayList<Button>();
        a.add(odpABtn);
        a.add(odpBBtn);
        a.add(odpCBtn);
        a.add(odpDBtn);

        switch(currentQuestion.getCorrectAnswer()) {
            case 'A':
                a.remove(0);
            break;

            case 'B':
                a.remove(1);
            break;

            case 'C':
                a.remove(2);
            break;

            case 'D':
                a.remove(3);
            break;
        }

        index = getRandomListIndex(a);
        setButtonUnactive(a.get(index));
        a.remove(index);

        index = getRandomListIndex(a);
        setButtonUnactive(a.get(index));
        a.remove(index);
    }

    private void setAllButtonsNormal() {
        setButtonNormal(odpABtn);
        setButtonNormal(odpBBtn);
        setButtonNormal(odpCBtn);
        setButtonNormal(odpDBtn);
    }

    private void getQuestion() {
        currentQuestion = AppData.getInstance().getRandomQuestion();
        setupQuestion();
    }

    private void setupQuestion() {
        odpABtn.setText(currentQuestion.getAnswerA());
        odpBBtn.setText(currentQuestion.getAnswerB());
        odpCBtn.setText(currentQuestion.getAnswerC());
        odpDBtn.setText(currentQuestion.getAnswerD());
        questionTxt.setText(currentQuestion.getQuestionText());
    }

    private void answerQuestion() {
        if(currentQuestion.getCorrectAnswer() == userAnswer) {
            if(currentPrize == 1000000) {
                AppData.getInstance().ClearUsedQUestions();
                finish();
            } else {
                nextQuestion();
            }
        } else {
            AppData.getInstance().ClearUsedQUestions();
            finish();
        }
    }

    private void nextQuestion() {
        setAllButtonsNormal();
        currentPrize = AppData.getInstance().getNextPrize();
        setPrizes();
        getQuestion();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.polNaPolBtn:
                setPolNaPol();
                polNaPolBtn.setVisibility(View.GONE);
            break;

            case R.id.pytanieDoPublicznosciBtn:
                setButtonPropose(getRandomButton());
                pytanieDoPublicznosciBtn.setVisibility(View.GONE);
            break;

            case R.id.teefonDoPrzyjacielaBtn:
                setButtonPropose(getRandomButton());
                teefonDoPrzyjacielaBtn.setVisibility(View.GONE);
            break;

            case R.id.odpABtn:
                userAnswer = 'A';
                answerQuestion();
            break;

            case R.id.odpBBtn:
                userAnswer = 'B';
                answerQuestion();
            break;

            case R.id.odpCBtn:
                userAnswer = 'C';
                answerQuestion();
            break;

            case R.id.odpDBtn:
                userAnswer = 'D';
                answerQuestion();
            break;
        }
    }
}
