package io.ss.milionerzy;

public class Question {
    private String questionText;
    private String answerA;
    private String answerB;
    private String answerC;
    private String answerD;
    private char correctAnswer;

    public Question(String text, String A, String B, String C, String D, char answer) {
        questionText = text;
        answerA = A;
        answerB = B;
        answerC = C;
        answerD = D;
        correctAnswer = answer;
    }

    public String getQuestionText() {
        return questionText;
    }

    public String getAnswerA() {
        return answerA;
    }

    public String getAnswerB() {
        return answerB;
    }

    public String getAnswerC() {
        return answerC;
    }

    public String getAnswerD() {
        return answerD;
    }

    public char getCorrectAnswer() {
        return correctAnswer;
    }
}

