package io.ss.milionerzy;

import java.util.ArrayList;
import java.util.Random;

class AppData {
    private static final AppData ourInstance = new AppData();
    private ArrayList<Question> questions = new ArrayList<Question>();
    private ArrayList<Integer> usedQuestions = new ArrayList<Integer>();
    private ArrayList<Integer> prizes = new ArrayList<Integer>();
    private int currentPrize = 0;

    public static AppData getInstance() {
        return ourInstance;
    }

    private AppData() {
        initQuestions();
        initPrizes();
    }

    public Integer getNextPrize() {
        return prizes.get(currentPrize++);
    }

    private void initPrizes() {
        prizes.add(100);
        prizes.add(200);
        prizes.add(300);
        prizes.add(500);
        prizes.add(1000);
        prizes.add(2000);
        prizes.add(4000);
        prizes.add(8000);
        prizes.add(16000);
        prizes.add(32000);
        prizes.add(64000);
        prizes.add(125000);
        prizes.add(250000);
        prizes.add(500000);
        prizes.add(1000000);
    }

    public Question getRandomQuestion() {
        Random rand = new Random();
        int n = rand.nextInt(questions.size());

        while(usedQuestions.contains(n)) {
            n = rand.nextInt(questions.size());
        }

        usedQuestions.add(n);
        return questions.get(n);
    }

    public void ClearUsedQUestions() {
        usedQuestions = new ArrayList<Integer>();
        currentPrize = 0;
    }

    private void initQuestions() {
        questions.add(new Question(
                "Krzysztof Jarzyna ze Szczecina to postac z jakiego polskiego filmu?",
                "Poklosie",
                "Roza",
                "Poranek Kojota",
                "Potop",
                'C'
        ));

        questions.add(new Question(
                "Ktory nie jest statkiem kosmicznym?",
                "Redstone",
                "Erenzy Lo",
                "Minotaur II",
                "GSLV-III",
                'B'
        ));

        questions.add(new Question(
                "Ktora marka nie produkuje ciagnikow?",
                "Massey Ferguson",
                "Lamborghini",
                "John Deere",
                "Jaguar",
                'D'
        ));

        questions.add(new Question(
                "Jakie dokladne wymiary ma kartka A4?",
                "210 x 297mm",
                "30 x 20cm",
                "31 x 26dm",
                "149 x 560mm",
                'A'
        ));

        questions.add(new Question(
                "Z jakiego kraju pochodzi firma Lenovo?",
                "Korea Poludniowa",
                "Japonia",
                "Chiny",
                "USA",
                'C'
        ));

        questions.add(new Question(
                "W jakim miescie znajduje sie Sky Tower?",
                "W Warszawie",
                "W Poznaniu",
                "We Wroclawiu",
                "W Krakowie",
                'C'
        ));

        questions.add(new Question(
                "Kredy w centrum Londynu doszlo do ataku terrorystow, ktorzy wysadzili jeden taki pojazd i 4 podziemne pociagi?",
                "2007",
                "2005",
                "2003",
                "2009",
                'B'
        ));

        questions.add(new Question(
                "W 2007 roku FSO zakonczylo produkcje",
                "Matiza",
                "Poloneza",
                "Malucha",
                "Lanosa",
                'A'
        ));

        questions.add(new Question(
                "Jaki jest pierwszy kraj na swiecie, ktory zalegalizowal eutanazje bez limitow wiekowych?",
                "Niemcy",
                "Wlochy",
                "Hiszpania",
                "Belgia",
                'D'
        ));

        questions.add(new Question(
                "W ile dni Apple sprzedal pierwsze cztery miliony telefonu iPhone 4S?",
                "w 31dni",
                "w 9dni",
                "w 3dni",
                "w 14dni",
                'D'
        ));

        questions.add(new Question(
                "Jaki kolor ma zazwyczaj czarna skrzynka w samolocie?",
                "Czarny",
                "Pomaranczowy",
                "Bialy",
                "Zielony",
                'B'
        ));

        questions.add(new Question(
                "W ktorym kraju siedzibe ma producent samolotow Airbus?",
                "Wielka Brytania",
                "Francja",
                "Niemcy",
                "USA",
                'B'
        ));

        questions.add(new Question(
                "W ktorym roku uruchomiono wyszukiwarke Google?",
                "1998",
                "1995",
                "2001",
                "2000",
                'B'
        ));

        questions.add(new Question(
                "Co to jest Steed w grze GTA IV?",
                "Bron",
                "Postac",
                "Ciezarowka",
                "Rodzaj misji",
                'C'
        ));

        questions.add(new Question(
                "Jak nazywa sie glowny bohater gry RPG akcji Mass Effect?",
                "Admiral Fields",
                "Komandor Shepard",
                "Sierzant Andersson",
                "Kapitan Bomba",
                'A'
        ));

        questions.add(new Question(
                "Jak nazywal sie pierwowzor Internetu?",
                "Facebook",
                "ARPANET",
                "Elgoog",
                "Google",
                'B'
        ));

        questions.add(new Question(
                "Jaka grecka litera widnieje W logo Wikipedii?",
                "Omega",
                "Ypsilon",
                "Alfa",
                "Gamma",
                'A'
        ));

        questions.add(new Question(
                "Jaki tytul nosi gra o braciach Mario wydana na konsole Nintendo Wii?",
                "New Super Mario Bros. WII",
                "New Super Mario Bros. WII",
                "Super Mario Bros. WII",
                "Super Mario WII",
                'B'
        ));
    }

}
